import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataService } from "../data.service";

@Component({
  selector: 'app-add',
  template: `
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8"/>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    </head>
    <body>
    <div class="container" style="text-align:center">
  
    <div class="form-control">
    <input class="form-control" [(ngModel)]="val1"  type="text" placeholder="Title">
    <textarea class="form-control" [(ngModel)]="val2"  type="text" placeholder="Description" maxlength="50"></textarea>
    <button class="btn btn-primary m-1" (click)="onClick(val1,val2)" >Add</button>
      </div>
    </div>
    </body>
    </html>
  `,
  styles: []
})
export class AddComponent implements OnInit {

  constructor(public data: DataService) { }

  ngOnInit() {

  }

  onClick(val1,val2){
        this.data.adddata(val1,val2);
    }

    }

