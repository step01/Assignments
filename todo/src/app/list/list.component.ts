import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import * as _ from "lodash";

@Component({
  selector: 'app-list',
  template: `
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8"/>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    </head>
    <body>
    <form>
    <div class="container">
      <div class="jumbotron p-1">
        <div class="form-control form-check" *ngFor="let d of data_list">
          <h3>
            Title: {{d.title}},<br> Description: {{d.desc}}
          </h3>
          <div class="form-check">
            <input type="checkbox" id={{d.id}} (click)="checkList(d.id)"> Done
            <div *ngIf="flag; then thenBlock;"></div>
            <button class="btn btn-primary" type="button" data-toggle="" data-target="#collapseExample" (click)=onClick1()>
            Edit
            </button>
            <ng-template #thenBlock>
            <div class="" id="collapseExample">
              <div class="form-control card card-body form-control">
                <input class="form-control" #myValue1  type="text" placeholder={{d.title}}>
                <textarea class="form-control" #myValue2  type="text" placeholder="Description" maxlength="50">{{d.title}}</textarea>
                <button class="btn btn-primary m-1" (click)="onClick(d.id,myValue1.value,myValue2.value)" >Add</button>
              </div> 
              </div>
              </ng-template>
              <ng-template #elseBlock>
              </ng-template>
          </div>
        </div>
      </div>
    </div>
    </form>
    </body>
    </html>
  `,
  styles: []
})
export class ListComponent implements OnInit {

 public data_list = [];
 public checked_id;;
 public flag=false;

  constructor(public data: DataService) { }

  ngOnInit() {
    this.data_list = this.data.getData();
  }

  checkList(id){
   let index = _.findIndex(this.data_list, function(o) { return o.id == id; });
   this.data_list.splice(index,1);
  }
  
  onClick1(){
  this.flag=true;
  }

  onClick(id,val1,val2){
        let index = _.findIndex(this.data_list, function(o) { return o.id == id; });
        this.data_list.splice(index,1);
        this.data.adddata(val1,val2);
        this.flag=false;
    }

}