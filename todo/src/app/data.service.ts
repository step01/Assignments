 import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

	data_arr = [];
	i = 0;
  constructor() { }

  
  adddata(val1: string,val2: string){
  	this.data_arr.push({"id":this.i,"title":val1,"desc":val2});
  	this.i++;
	}

	getData(){
		return this.data_arr;
	}
}